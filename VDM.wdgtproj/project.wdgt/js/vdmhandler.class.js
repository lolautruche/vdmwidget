/**
 * @class VDMHandler
 * @author Jerome Vieilledent
 * @constructor
 */
function VDMHandler() {
	this.remoteVDM = null; // Reponse du serveur de l'API VDM convertie en JSON
	this.lastUpdated = 0; // Timestamp de derniere mise a jour
	this.curPos = 0; // Lors de l'affichage en mode "last", indique sur quelle VDM on se trouve
	this.modeSelectMenu = document.getElementById('VDMMode').object; // On utilise document.getElementById() pour acceder a l'objet Apple
	this.version = this.getVersion();
	
	// Mode du Widget (last ou random). On va chercher dans les preferences du widget.
	// Si la preference n'existe pas (premier lancement), on charge le mode par defaut et on l'enregistre comme preference
	var mode = widget.preferenceForKey('VDMMode');
	if (mode) {
		this.VDMMode = mode;
	} else {
		this.VDMMode = VDMHandler.Params.Mode.Random;
		widget.setPreferenceForKey(this.VDMMode, 'VDMMode');
	}
	
	// Autorisation de la verification des mises a jour
	// true par defaut
	var prefAllowUpdate = widget.preferenceForKey('allowCheckUpdate');
	if (prefAllowUpdate != undefined) {
		this.allowCheckUpdate = prefAllowUpdate == 'true' ? true : false;
	} else {
		this.allowCheckUpdate = true;
		widget.setPreferenceForKey('true', 'allowCheckUpdate');
	}
	// On check les mise a jour si autorise
	if (this.allowCheckUpdate)
		this.checkUpdate();
	
	this.modeSelectMenu.setSelectedIndex(this.VDMMode);
	$(VDMHandler.Params.Selectors.Version).text('Version '+this.version);
	this.displayToolbar();
	this.getRemoteVDM();
}

/*
 * Methodes membres
 */
VDMHandler.prototype = {

	/**
	 * Appelle l'API VDM pour recuperer la ou les VDM en fonction du mode choisi
	 */
	getRemoteVDM: function() {
		$(VDMHandler.Params.Selectors.Loader).show();

		switch (this.VDMMode) {
			case VDMHandler.Params.Mode.Last: // Mode Last
				var VDMUrl = VDMHandler.Params.General.URLLast;
				break;
			
			case VDMHandler.Params.Mode.Random: // Mode Random
				var VDMUrl = VDMHandler.Params.General.URLRandom;
				break;
				
			default: 
				var VDMUrl = VDMHandler.Params.General.URLRandom;
				break;
		}
		
		VDMUrl = VDMUrl + '?key=' + VDMHandler.Params.General.APIKey;
		
		// On va chercher les resultats. Ils seront evalues dans la callback onVDMLoad
		$.ajax({
			type: 'GET',
			url: VDMUrl,
			dataType: 'xml',
			success: VDMHandler.Static.onVDMLoad,
			error: function() {
				$(VDMHandler.Params.Selectors.Loader).hide();
				VDMHandler.Static.handleError(VDMHandler.Messages.Error.noConnection);
			}
		});
		
	},
	
	/**
	 * Verification des mises a jour
	 */
	checkUpdate: function() {
		var inst = this;
		$.ajax({
			type: 'GET',
			url: VDMHandler.Params.General.URLUpdateCheck,
			dataType: 'xml',
			success: function(XMLResponse) {
				var JSONData = $.xmlToJSON(XMLResponse);
				var version = JSONData.release[0].version;
				
				if (version != inst.version) {
					$(VDMHandler.Params.Selectors.newVersion).text(version);
					$(VDMHandler.Params.Selectors.downloadUpdate).attr('href', JSONData.release[0].url);
					$(VDMHandler.Params.Selectors.changelog).html(JSONData.release[0].changelog[0].Text);
					$(VDMHandler.Params.Selectors.updateInfo).show();
					$(VDMHandler.Params.Selectors.updateInfo).get(0).object.refresh();
				}
			},
			error: function() {
				alert('error');
			}
		});
	},
	
	/**
	 * Change de mode VDM
	 */
	changeMode: function(mode) {
		widget.setPreferenceForKey(mode, 'VDMMode');
		this.VDMMode = mode;
		this.curPos = 0; // Init de la position courante
		
		// Affichage de la toolbar
		$(VDMHandler.Params.Selectors.ToolbarLast).toggle();
		$(VDMHandler.Params.Selectors.ToolbarRandom).toggle();
		
		var inst = this;
		$(VDMHandler.Params.Selectors.TextContent).fadeOut('slow', function() {
			inst.getRemoteVDM();
		});
		
		delete inst;
	},
	
	/**
	 * Affiche la bonne toolbar en fonction du mode choisi
	 */
	displayToolbar: function() {
		var element = null;
		
		switch (this.VDMMode) {
			case VDMHandler.Params.Mode.Random: 
				element = $(VDMHandler.Params.Selectors.ToolbarRandom);
				break;
				
			case VDMHandler.Params.Mode.Last:
				element = $(VDMHandler.Params.Selectors.ToolbarLast)
				break;
				
			default:
				element = $(VDMHandler.Params.Selectors.ToolbarRandom);
				break;
		}
		
		element.show();
	},
	
	/**
	 * Recupere la VDM suivante en mode "last"
	 * 
	 * @param {HTMLDivElement} Bouton clique pour appeler cette methode. sa propriete object est une instance de AppleButton
	 */
	getNextVDM: function(clickedButton) {
		if (clickedButton.object.enabled) {
			this.curPos++;
			this._enableNextPrevButtons();
			this.displayVDM(this.curPos);
		}
	},
	
	/**
	 * Recupere la VDM precedente en mode "last"
	 * 
	 * @param {HTMLDivElement} Bouton clique pour appeler cette methode. sa propriete object est une instance de AppleButton
	 */
	getPrevVDM: function(clickedButton) {
		if (clickedButton.object.enabled) {
			this.curPos--;
			this._enableNextPrevButtons();
			this.displayVDM(this.curPos);
		}
	},
	
	refreshLastVDM: function() {
		this.curPos = 0; // Reinit de la position courante
		this._disableNextPrevButtons();
		
		var inst = this;
		$(VDMHandler.Params.Selectors.TextContent).fadeOut('slow', function() {
			inst.getRemoteVDM();
		});
		
		delete inst;
	},
	
	/**
	 * Affiche la VDM demandee
	 * 
	 * @param {int} Position de la VDM dans le flux. 0 par defaut
	 */
	displayVDM: function(pos) {
		var pos = pos ? pos : 0;
		
		$(VDMHandler.Params.Selectors.TextContent).fadeOut('slow', function() {
			$(VDMHandler.Params.Selectors.TextContent).text(VDM.remoteVDM.vdms[0].vdm[pos].texte[0].Text);
			$(VDMHandler.Params.Selectors.Author).text(VDM.remoteVDM.vdms[0].vdm[pos].auteur[0].Text);
			
			$(VDMHandler.Params.Selectors.TextContent).fadeIn('slow');
		});
		
	},
	
	/**
	 * Gestion de l'activation / desactivation des boutons "suivant" / "precedent"
	 */
	_enableNextPrevButtons: function() {
		var VDMCount = this.remoteVDM.vdms[0].vdm.length;
		var prevButtonObject = document.getElementById('previous').object;
		var nextButtonObject = document.getElementById('next').object;
		
		// Desactivation du bouton "precedent" si on est deja sur la premiere VDM
		if(this.curPos > 0) {
			prevButtonObject.setEnabled(true);
			$('#previous').removeClass('disabled');
			
		} else {
			prevButtonObject.setEnabled(false);
			$('#previous').addClass('disabled');
		}
		
		// Desactivation du bouton "suivant" si on est sur la derniere VDM
		if (this.curPos < (VDMCount - 1)) {
			nextButtonObject.setEnabled(true);
			$('#next').removeClass('disabled');
		} else {
			nextButtonObject.setEnabled(false);
			$('#next').addClass('disabled');
		}
	},
	
	/**
	 * Permet de desactiver les boutons suivant et precedent
	 */
	_disableNextPrevButtons: function() {
		var prevButtonObject = document.getElementById('previous').object;
		var nextButtonObject = document.getElementById('next').object;
		
		prevButtonObject.setEnabled(false);
		$('#previous').removeClass('disabled');
		
		nextButtonObject.setEnabled(false);
		$('#next').addClass('disabled');
	},
	
	getVersion: function() {
		return VDMHandler.Static.getInfo('CFBundleVersion');
	}
}

/*
 * Methodes et proprietes statiques
 * Necessite que l'objet VDM soit deja declare
 */
VDMHandler.Static = {
	
	onVDMLoad: function(XMLResponse) {
		VDMText.lastUpdated = (new Date).getTime();
		VDM.remoteVDM = $.xmlToJSON(XMLResponse);
		var errorCode = VDM.remoteVDM.code[0].Text;
		$(VDMHandler.Params.Selectors.Loader).hide();
		
		if (errorCode == 0) { // Erreur retournee par le webservice
			VDMHandler.Static.handleError(VDM.remoteVDM.erreurs[0].erreur[0].Text);
			$(VDMHandler.Params.Selectors.TextContent).fadeIn('slow');
		} else {
			VDM.displayVDM(VDM.curPos);
			
			if (VDM.VDMMode == VDMHandler.Params.Mode.Last) { // Activation des boutons suivant/precedent si mode 'last'
				VDM._enableNextPrevButtons();
				VDM.lastUpdated = (new Date).getTime();
			}
		}
	},
	
	handleError: function(message) {
		$(VDMHandler.Params.Selectors.TextContent).html('<strong>Erreur</strong> : <br />'+message);
		$(VDMHandler.Params.Selectors.TextContent).fadeIn('slow');
	},
	
	getInfo: function(key) {
		var key = key ? key : "CFBundleVersion";
		var xmlReq = new XMLHttpRequest(); 
		xmlReq.open("GET", "Info.plist", false); 
		xmlReq.send(null); 
    
		var xml = xmlReq.responseXML; 
		var keys = xml.getElementsByTagName("key");
		var returnInfo = null;

		for(i=0; i<keys.length; i++){
			if(key == keys[i].firstChild.data){
				returnInfo = keys[i].nextSibling.nextSibling.firstChild.data;
				break;
			}
		}

		return returnInfo; 
	}
}

/*
 * Parametres
 */
VDMHandler.Params = {
	General: {
		URLRandom: 'http://api.viedemerde.fr/1.2/view/random/',
		URLLast: 'http://api.viedemerde.fr/1.2/view/last/',
		URLUpdateCheck: 'http://www.lolart.net/VDM/version.xml',
		APIKey: '48a93a01c5a27',
		LastMaxVDM: 15
	},
	Mode: { // Indexes du menu deroulant
		Random: '0',
		Last: '1'
	},
	Vote: {
		URLVote: 'http://api.viedemerde.fr/1.0/vote/',
		valide: 'je_valide',
		merite: 'bien_merite'
	},
	Selectors: {
		TextContent: '#VDMText',
		Author: '#VDMAuthor',
		Date: '#VDMDate',
		RandomReloadButton: '#RandomReload',
		ToolbarRandom: '#toolbarRandom',
		ToolbarLast: '#toolbarLast',
		Loader: '#VDMLoader',
		Version: '#VDMVersion',
		updateInfo: '#updateInfo',
		newVersion: '#updateInfo #newVersion',
		changelog: '#updateInfo #changelog',
		downloadUpdate: '#updateInfo #updateURL',
		ignoreUpdate: '#updateInfo #closeUpdate',
		checkboxPref: '#back #checkboxUpdate'
	}
};

/*
 * Messages
 */
VDMHandler.Messages = {
	Error: {
		noConnection: 'Impossible de se connecter au site VDM'
	}
};

/*
 * Exceptions
 */
function VDMHandlerException(message) {
	this.name = 'VDMHandlerException';
	this.message = message;
}