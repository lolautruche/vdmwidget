/*
 * Initialisation
 */
var VDM = null;
$(document).ready(function() {
	
	setupParts(); // Initialisation des elements graphiques
	VDM = new VDMHandler();
	
	// Ecoute des evenements
	$(VDMHandler.Params.Selectors.RandomReloadButton).click(function() {
		$(VDMHandler.Params.Selectors.TextContent).fadeOut('slow', function() {
			VDM.getRemoteVDM();
		});
	});
	
	$('#previous').click(function() {
		VDM.getPrevVDM(this);
	});
	
	$('#next').click(function() {
		VDM.getNextVDM(this);
	});
	
	$('#refresh').click(function() {
		VDM.refreshLastVDM();
	});
	
	$('#VDMSite').click(function() {
		widget.openURL($(this).text());
	});
	
	$('#LolartSite').click(function() {
		widget.openURL($(this).text());
	});
	
	// Mise a jour
	$(VDMHandler.Params.Selectors.downloadUpdate).click(function() {
		widget.openURL($(this).attr('href'));
	});
	$(VDMHandler.Params.Selectors.ignoreUpdate).click(function() {
		$(VDMHandler.Params.Selectors.updateInfo).hide();
	});
	
	// Preference de mise a jour
	var prefAllowUpdate = widget.preferenceForKey('allowCheckUpdate');
	if (prefAllowUpdate == 'true')
		$(VDMHandler.Params.Selectors.checkboxPref).attr('checked', 'checked');
	else
		$(VDMHandler.Params.Selectors.checkboxPref).removeAttr('checked');
	$(VDMHandler.Params.Selectors.checkboxPref).click(function() {
		if ($(this).attr('checked')) {
			widget.setPreferenceForKey('true', 'allowCheckUpdate');
		} else {
			widget.setPreferenceForKey('false', 'allowCheckUpdate');
		}
	});
	
});

function VDMChangeMode(evt) {
	var newMode = this.getValue(); // this fait reference au select
	VDM.changeMode(newMode);
}

//
// Function: load()
// Called by HTML body element's onload event when the widget is ready to start
//
function load()
{
    
}

//
// Function: remove()
// Called when the widget has been removed from the Dashboard
//
function remove()
{
    // Stop any timers to prevent CPU usage
    // Remove any preferences as needed
    // widget.setPreferenceForKey(null, createInstancePreferenceKey("your-key"));
}

//
// Function: hide()
// Called when the widget has been hidden
//
function hide()
{
    // Stop any timers to prevent CPU usage
}

//
// Function: show()
// Called when the widget has been shown
//
function show()
{
    try {
		if (VDM.VDMMode == VDMHandler.Params.Mode.Last) { // On ne met a jour qu'en mode last
			// Refresh feed if 15 minutes have passed since the last update
			var now = (new Date).getTime();
			if ((now - VDM.lastUpdated) > 15 * 60 * 1000) {
				VDM.refreshLastVDM();
    }
		}
	} catch (e) {
		alert(e.message);
	}
}

//
// Function: sync()
// Called when the widget has been synchronized with .Mac
//
function sync()
{
    // Retrieve any preference values that you need to be synchronized here
    // Use this for an instance key's value:
    // instancePreferenceValue = widget.preferenceForKey(null, createInstancePreferenceKey("your-key"));
    //
    // Or this for global key's value:
    // globalPreferenceValue = widget.preferenceForKey(null, "your-key");
}

//
// Function: showBack(event)
// Called when the info button is clicked to show the back of the widget
//
// event: onClick event from the info button
//
function showBack(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

//
// Function: showFront(event)
// Called when the done button is clicked from the back of the widget
//
// event: onClick event from the done button
//
function showFront(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }

    front.style.display="block";
    back.style.display="none";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}
